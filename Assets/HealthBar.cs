﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    Image healthImage;
    public Text healthText;
    // Start is called before the first frame update
    void Start()
    {
        healthImage = this.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        healthImage.fillAmount = Mathf.PingPong(Time.time * 0.3f, 1);
        float hp = Mathf.Round(healthImage.fillAmount * 100);
        healthText.text = hp.ToString()+"%";
        
    }
}
