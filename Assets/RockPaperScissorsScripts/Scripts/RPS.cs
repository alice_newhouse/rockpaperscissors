﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public enum Choices
{
    None,
    Fork,
    Spoon,
    Knife
}
public class RPS : MonoBehaviour
{

    private bool isPressed;

    private Choices randomNumber;
    private Choices playerChoice;


    [Header("Option One Variables")]
    public Sprite optionOneImage;

    [Space(10)]
    [Header("Option Two Variables")]
    public Sprite optionTwoImage;

    [Space(10)]
    [Header("Option Three Variables")]
    public Sprite optionThreeImage;

    [Space(10)]
    [Header("GUI Objects")]

    public Sprite fighterOne;
    public Sprite fighterTwo;
    public Sprite fighterThree;

    public Button optionOneButton;
    public Button optionTwoButton;
    public Button optionThreeButton;
    public Button BattleButton;

    public Text gameOutputText;
    public Text playerLifeCounter;
    public Text enemyLifeCounter;
    public Text BattleButtonText;

    public GameObject playerChoiceImage;
    public GameObject opponentChoiceImage;

    // Caches/applies the LifeManager to the GameManager//
    public GameObject GO_PlayerLifeManager;
    private LifeManager PlayerLifeManager;
    public GameObject GO_EnemyLifeManager;
    private LifeManager EnemyLifeManager;

    // Caches/applies the ResultsDisplayManager to the GameManager//
    public GameObject GO_ResultsDisplayManager;
    private ResultScreenManager ResultsDisplayManager;

    // Caches/applies the OvenDoorManager to GameManager //
    public GameObject GO_OvenDoorManager;
    private OvenDoorManager OvenDoorManager;

    private Choices PlayerSelection;
    private Choices EnemySelection;

    /*
     * 
     * CREATE YOUR CUSTOM UNITY EVENT HERE
     * 
     */
    public UnityEvent OvenTimer;


    /*
     * 
     * 
     * 
     */

    bool gameOver = false;




    // Use this for initialization
    void Start()
    {
        // Caching LifeManagers to Start function - Debugging in the case that the Manager is not found //
        PlayerLifeManager = GO_PlayerLifeManager.GetComponent<LifeManager>();
        Debug.Assert(PlayerLifeManager != null);
        EnemyLifeManager = GO_EnemyLifeManager.GetComponent<LifeManager>();
        Debug.Assert(EnemyLifeManager != null);

        // Caching ResultsDisplayManager to Start function - Debugging in the case that the Manager is not found //
        ResultsDisplayManager = GO_ResultsDisplayManager.GetComponent<ResultScreenManager>();
        Debug.Assert(ResultsDisplayManager != null);

        // Caching OvenDoorManager to Start function - Debugging in the case that the Manager is not found // 
        OvenDoorManager = GO_OvenDoorManager.GetComponent<OvenDoorManager>();
        Debug.Assert(OvenDoorManager != null);






        optionOneButton.GetComponent<Image>().sprite = optionOneImage;
        optionTwoButton.GetComponent<Image>().sprite = optionTwoImage;
        optionThreeButton.GetComponent<Image>().sprite = optionThreeImage;








        SetUpGame();
        playerChoice = 0;

    }

    private void SetUpGame()
    {
        gameOutputText.text = "Pick a utensil: " + (Choices)1 + ", " + (Choices)2 + " or " + (Choices)3;
        playerLifeCounter.text = PlayerLifeManager.GetLifePoints().ToString();
        enemyLifeCounter.text = EnemyLifeManager.GetLifePoints().ToString();

    }

    public void ClickButton(int buttonClicked)
    {
        playerChoiceImage.SetActive(true);
        OvenDoorManager.SetPlayerDoorOpen(true);

        if (buttonClicked == 1)
        {
            playerChoice = Choices.Fork;
            gameOutputText.text = "You have chosen " + (Choices)1;
            playerChoiceImage.GetComponent<Image>().sprite = fighterOne;

        }

        if (buttonClicked == 2)
        {
            playerChoice = Choices.Spoon;
            gameOutputText.text = "You have chosen " + (Choices)2;
            playerChoiceImage.GetComponent<Image>().sprite = fighterTwo;

        }

        if (buttonClicked == 3)
        {
            playerChoice = Choices.Knife;
            gameOutputText.text = "You have chosen " + (Choices)3;
            playerChoiceImage.GetComponent<Image>().sprite = fighterThree;

        }


    }

    private void Update()
    {

    }

    /// <summary>
    /// Applying the instance in which a life is decremented (win/loss)
    /// -and- Applying the instance of when win/loss/draw text display is relevant
    /// </summary>
    /// <param name="playerChoice"></param>
    /// <param name="enemyChoice"></param>
    private void DoBattle(Choices enemyChoice)
    {
        OvenDoorManager.SetEnemyDoorOpen(true);

        opponentChoiceImage.SetActive(true);

        if (playerChoice == enemyChoice)
        {

            gameOutputText.text += "\n You chose the same! DRAW!";
            ResultsDisplayManager.ActiveResult(Results.Draw);
        }
        else
        {
            // Simplified the if stack for the win/loss state //

            if (CheckPlayerWon(enemyChoice))
            {
                gameOutputText.text += "\n " + playerChoice + " beats " + enemyChoice + ". You WIN!";
                EnemyLifeManager.DecrementLifePoints();
                ResultsDisplayManager.ActiveResult(Results.Win);
            }
            else
            {
                gameOutputText.text += "\n " + playerChoice + " beats " + enemyChoice + ". You LOSE!";
                PlayerLifeManager.DecrementLifePoints();
                ResultsDisplayManager.ActiveResult(Results.Loss);
            }
        }

        // gameOutputText.text += "\n\n Choose a utensil: " + playerChoice + ", " + choiceTwoText + " or " + choiceThreeText;
        playerLifeCounter.text = PlayerLifeManager.GetLifePoints().ToString();
        enemyLifeCounter.text = EnemyLifeManager.GetLifePoints().ToString();

        if (PlayerLifeManager.GetLifePoints() == 0)
        {
            gameOutputText.text = "YOU LOSE!";
            BattleButtonText.text = "Reset";
            gameOver = true;
        }
        else if (EnemyLifeManager.GetLifePoints() == 0)
        {

            gameOutputText.text = "YOU WON!";
            BattleButtonText.text = "Reset";
            gameOver = true;
        }


        


    }

    /// <summary>
    /// Function - Identifies if the player has won (comapring enum to enum)
    /// </summary>
    /// <param name="enemyChoice"></param>

    private bool CheckPlayerWon(Choices enemyChoice)
    {
        Debug.Assert(playerChoice != enemyChoice);
        if (playerChoice == Choices.Spoon && enemyChoice == Choices.Knife)
        {
            return true;
        } 
        else if (playerChoice == Choices.Knife && enemyChoice == Choices.Spoon)
        {
            return true;
        }
        else if (playerChoice == Choices.Fork && enemyChoice == Choices.Knife)
        {
            return true;
        }
        return false;
    }

    public void ClickBattle()
    {
        if (gameOver)
            resetBoard();


        randomNumber = (Choices)Random.Range(1, 4);
        setOpponentImage(randomNumber);
        DoBattle(randomNumber);

        /*
         * 
         * 
         * INVOKE YOUR EVENT HERE
         */

        OvenTimer.Invoke();

        /*
         * 
         * 
         * 
         */

    }

    public void setOpponentImage(Choices choice)
    {

        if (choice == Choices.Fork)
            opponentChoiceImage.GetComponent<Image>().sprite = fighterOne;
        if (choice == Choices.Spoon)
            opponentChoiceImage.GetComponent<Image>().sprite = fighterTwo;
        if (choice == Choices.Knife)
            opponentChoiceImage.GetComponent<Image>().sprite = fighterThree;
    }

    public void resetBoard()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}


