﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvenDoorManager : MonoBehaviour
{
    public GameObject PlayerOvenDoorClosed;
    public GameObject PlayerOvenDoorOpen;
    public GameObject EnemyOvenDoorClosed;
    public GameObject EnemyOvenDoorOpen;

    public void SetPlayerDoorOpen(bool newisopen)
    {
        PlayerOvenDoorClosed.SetActive(!newisopen);
        PlayerOvenDoorOpen.SetActive(newisopen);
    }

    public void SetEnemyDoorOpen(bool newisopen)
    {
        EnemyOvenDoorClosed.SetActive(!newisopen);
        EnemyOvenDoorOpen.SetActive(newisopen);
    }

    // Start is called before the first frame update
    void Start()
    {
        SetPlayerDoorOpen(false);
        SetEnemyDoorOpen(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
