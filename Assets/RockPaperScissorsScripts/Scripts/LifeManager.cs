﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeManager : MonoBehaviour
{
    /// <summary>
    /// Defining life points as 3
    /// </summary>
    private int LifePoints = 3;

    public GameObject[] LifeGraphics;
    
    /// <summary>
    /// Returns the current life points of this character
    /// </summary>
    public int GetLifePoints()
    {
        return LifePoints;

    }
    
    /// <summary>
    /// Decreases the current life points and updates graphics appropriately
    /// </summary>
    public void DecrementLifePoints()
    {
        LifePoints--;
        Debug.Assert(LifePoints >= 0);
        UpdateLifeGraphics();
    }

    /// <summary>
    /// Sets each element of Life Graphics active depending on whether each graphic is appropriate for the number of life points
    /// </summary>
    private void UpdateLifeGraphics()
    {
        for (int index = 0; index < LifeGraphics.Length; index++)
        {
            bool active = index <= LifePoints - 1;
            LifeGraphics[index].SetActive(active);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
