﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.SceneManagement;



public class OldRPS : MonoBehaviour
{

    private int randomNumber;
    private int playerChoice;


    [Header("Option One Variables")]

    public string optionOneName;
    public Sprite optionOneImage;

    [Space(10)]
    [Header("Option Two Variables")]
    public string optionTwoName;
    public Sprite optionTwoImage;

    [Space(10)]
    [Header("Option Three Variables")]
    public string optionThreeName;
    public Sprite optionThreeImage;

    [Space(10)]
    [Header("GUI Objects")]

    public Button optionOneButton;
    public Button optionTwoButton;
    public Button optionThreeButton;

    public Text gameOutputText;
    public Text playerLifeCounter;
    public Text enemyLifeCounter;
    public Text BattleButtonText;

    public GameObject playerChoiceImage;
    public GameObject opponentChoiceImage;

    // Caches/applies the LifeManager to the GameManager//
    public GameObject GO_PlayerLifeManager;
    private LifeManager PlayerLifeManager;
    public GameObject GO_EnemyLifeManager;
    private LifeManager EnemyLifeManager;

    // Caches/applies the ResultsDisplayManager to the GameManager//
    public GameObject GO_ResultsDisplayManager;
    private ResultScreenManager ResultsDisplayManager;

    /*
     * 
     * CREATE YOUR CUSTOM UNITY EVENT HERE
     * 
     */
    public UnityEvent OvenTimer;


    /*
     * 
     * 
     * 
     */

    string choiceOneText;
    string choiceTwoText;
    string choiceThreeText;

    bool gameOver = false;




    // Use this for initialization
    void Start()
    {
        // Caching LifeManagers to Start function - Debugging in the case that the Manager is not found //
        PlayerLifeManager = GO_PlayerLifeManager.GetComponent<LifeManager>();
        Debug.Assert(PlayerLifeManager != null);
        EnemyLifeManager = GO_EnemyLifeManager.GetComponent<LifeManager>();
        Debug.Assert(EnemyLifeManager != null);

        // Caching ResultsDisplayManager to Start function - Debugging in the case that the Manager is not found //
        ResultsDisplayManager = GO_ResultsDisplayManager.GetComponent<ResultScreenManager>();
        Debug.Assert(ResultsDisplayManager != null);

        choiceOneText = optionOneName;
        choiceTwoText = optionTwoName;
        choiceThreeText = optionThreeName;






        optionOneButton.GetComponent<Image>().sprite = optionOneImage;
        optionTwoButton.GetComponent<Image>().sprite = optionTwoImage;
        optionThreeButton.GetComponent<Image>().sprite = optionThreeImage;








        SetUpGame();
        playerChoice = 0;

    }

    private void SetUpGame()
    {
        gameOutputText.text = "Choose a utensil: " + choiceOneText + ", " + choiceTwoText + " or " + choiceThreeText;
        playerLifeCounter.text = PlayerLifeManager.GetLifePoints().ToString();
        enemyLifeCounter.text = EnemyLifeManager.GetLifePoints().ToString();

    }

    public void ClickButton(int buttonClicked)
    {
        playerChoiceImage.SetActive(true);


        if (buttonClicked == 1)
        {
            playerChoice = 1;
            gameOutputText.text = "You have chosen " + choiceOneText;
            playerChoiceImage.GetComponent<Image>().sprite = optionOneImage;

        }

        if (buttonClicked == 2)
        {
            playerChoice = 2;
            gameOutputText.text = "You have chosen " + choiceTwoText;
            playerChoiceImage.GetComponent<Image>().sprite = optionTwoImage;

        }

        if (buttonClicked == 3)
        {
            playerChoice = 3;
            gameOutputText.text = "You have chosen " + choiceThreeText;
            playerChoiceImage.GetComponent<Image>().sprite = optionThreeImage;

        }


    }


    private void Update()
    {

    }

    /// <summary>
    /// Applying the instance in which a life is decremented (win/loss)
    /// -and- Applying the instance of when win/loss/draw text display is relevant
    /// </summary>
    /// <param name="playerChoice"></param>
    /// <param name="enemyChoice"></param>
    private void DoBattle(int playerChoice, int enemyChoice)
    {

        opponentChoiceImage.SetActive(true);

        if (playerChoice == enemyChoice)
        {

            gameOutputText.text += "\n You chose the same! Draw!";
            ResultsDisplayManager.ActiveResult(Results.Draw);
        }
        else
        {
            // Simplified the if stack for the win/loss state //

            if (CheckPlayerWon(enemyChoice))
            {
                gameOutputText.text += "\n Your " + choiceTwoText + " beats " + choiceOneText + ". You Win!";
                EnemyLifeManager.DecrementLifePoints();
                ResultsDisplayManager.ActiveResult(Results.Win);
            }
            else
            {
                gameOutputText.text += "\n Your " + choiceOneText + " loses to Enemy " + choiceTwoText + ". You Lose!";
                PlayerLifeManager.DecrementLifePoints();
                ResultsDisplayManager.ActiveResult(Results.Loss);
            }
        }
      

        gameOutputText.text += "\n\n Choose a utensil: " + choiceOneText + ", " + choiceTwoText + " or " + choiceThreeText;
        playerLifeCounter.text = PlayerLifeManager.GetLifePoints().ToString();
        enemyLifeCounter.text = EnemyLifeManager.GetLifePoints().ToString();

        if (PlayerLifeManager.GetLifePoints() == 0)
        {
            gameOutputText.text = "YOU LOSE!";
            BattleButtonText.text = "Reset";
            gameOver = true;
        }
        else if (EnemyLifeManager.GetLifePoints() == 0)
        {

            gameOutputText.text = "YOU WON!";
            BattleButtonText.text = "Reset";
            gameOver = true;
        }


        


    }

    /// <summary>
    /// Function - Identifies if the player has won
    /// </summary>
    /// <param name="enemyChoice"></param>

    private bool CheckPlayerWon(int enemyChoice)
    {
        Debug.Assert(playerChoice != enemyChoice);
        if (playerChoice == 2 && enemyChoice == 1)
        {
            return true;
        } 
        else if (playerChoice == 3 && enemyChoice == 2)
        {
            return true;
        }
        else if (playerChoice == 1 && enemyChoice == 3)
        {
            return true;
        }
        return false;
    }
    
    public void ClickBattle()
    {
        if (gameOver)
            resetBoard();


        randomNumber = Random.Range(1, 4);
        setOpponentImage(randomNumber);
        DoBattle(playerChoice, randomNumber);

        /*
         * 
         * 
         * INVOKE YOUR EVENT HERE
         */

        OvenTimer.Invoke();

        /*
         * 
         * 
         * 
         */

    }

    public void setOpponentImage(int choice)
    {

        if (choice == 1)
            opponentChoiceImage.GetComponent<Image>().sprite = optionOneImage;
        if (choice == 2)
            opponentChoiceImage.GetComponent<Image>().sprite = optionTwoImage;
        if (choice == 3)
            opponentChoiceImage.GetComponent<Image>().sprite = optionThreeImage;
    }

    public void resetBoard()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}


