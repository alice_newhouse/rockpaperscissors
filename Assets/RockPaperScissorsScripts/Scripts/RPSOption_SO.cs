﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[CreateAssetMenu]
public class RPSOption : ScriptableObject
{
    public string optionName;
    public Sprite optionImage;

    public RPSOption init(string name, Sprite image)
    {
        optionName = name;
        optionImage = image;
        return this;
    }

}