﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Allows only one text option to be displayed at one time
/// </summary>
public enum Results
{
    Win,
    Loss,
    Draw
}

public class ResultScreenManager : MonoBehaviour
{
    public GameObject Win;
    public GameObject Loss;
    public GameObject Draw;

    private const float DisplayTime = 2;
    private float TextActiveTimeCounter;
    private bool ResultActive;

    public void ActiveResult(Results ResultToActivate)
    {
        if (ResultActive)
        {
            ResetScreen();
        }
        switch (ResultToActivate)
        {
            case Results.Win:
                ResultActive = true;
                Win.SetActive(true);
                break;

            case Results.Loss:
                ResultActive = true;
                Loss.SetActive(true);
                break;

            case Results.Draw:
                ResultActive = true;
                Draw.SetActive(true);
                break;

            default:

                Debug.LogError(ResultToActivate);
                break;
        }
    }

    private void DeactivateAllResults()
    {
        Win.SetActive(false);
        Loss.SetActive(false);
        Draw.SetActive(false);
    }

    private void ResetScreen()
    {
        ResultActive = false;
        DeactivateAllResults();
        TextActiveTimeCounter = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        DeactivateAllResults();
    }

    // Update is called once per frame
    void Update()
    {
        if (ResultActive)
        {
            TextActiveTimeCounter += Time.deltaTime;
            if (TextActiveTimeCounter >= DisplayTime)
            {
                ResultActive = false;
                DeactivateAllResults();
                TextActiveTimeCounter = 0;
            }
        }
    }
}
