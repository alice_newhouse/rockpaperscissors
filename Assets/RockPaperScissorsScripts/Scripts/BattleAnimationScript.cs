﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BattleAnimationScript : MonoBehaviour {

	public UnityEvent hit;
	public UnityEvent end;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void End (){
		end.Invoke ();
	}

	public void Hit (){
		hit.Invoke ();
	}
}
